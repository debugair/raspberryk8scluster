## scp yaml to master node
```bash
ssh node1 sudo mkdir ~/docker
ssh node1 sudo chown ubuntu:ubuntu ~/docker
scp D:\Code\RaspberryK8s\_clean\Docker\* node1:~/docker
```

### build JupyterHub
The following script will build the docker image, tag it for the hub.docker.com repository and push the image. Like that it is available on all the nodes by downloading it from docker.
```bash
cd ~/docker
sudo docker build -f ~/docker/JupyterHub.Dockerfile -t jupyter-hub:latest .
sudo docker tag jupyter-hub:latest debugair/jupyter-hub:latest
sudo docker login docker.io
sudo docker push debugair/jupyter-hub:latest
```