#https://towardsdatascience.com/setup-your-home-jupyterhub-on-a-raspberry-pi-7ad32e20eed
#docker run -it --entrypoint "/bin/bash" ubuntu:20.04
#sudo docker build -f /home/ubuntu/JupyterHub.Dockerfile -t jupyter-hub:latest .

FROM ubuntu:20.04


# UPDATE AND BASE PACKAGES
RUN apt update
RUN apt -y install curl


# INSTALL NPM
RUN DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends npm
RUN node -e "console.log('Running Node.js ' + process.version)"


# NPM MODULES
RUN npm install -g configurable-http-proxy


# INSTALL PYTHON
RUN apt -y install python3
RUN apt -y install python3-pip
#RUN pip3 install --upgrade pip


# JUPYTER HUB
RUN pip3 install jupyterhub
RUN pip3 install notebook
RUN jupyterhub --generate-config
RUN mv jupyterhub_config.py /root


# JAVA
RUN apt -y install default-jdk
RUN java --version


# SPARK
RUN cd /opt && curl -sSL https://downloads.apache.org/spark/spark-3.0.1/spark-3.0.1-bin-hadoop3.2.tgz | tar -xzf -


# BUILD ENTRY POINT
RUN touch /opt/entrypoint.sh
RUN echo '#!/bin/sh' >> /opt/entrypoint.sh
RUN echo '#################################################################' >> /opt/entrypoint.sh
RUN echo '# JUPYTERHUB' >> /opt/entrypoint.sh
RUN echo '#################################################################' >> /opt/entrypoint.sh
RUN echo 'useradd -m -p $(openssl passwd -1 ${JUPYTER_PASSWORD}) -s /bin/bash -G sudo ${JUPYTER_USERNAME}' >> /opt/entrypoint.sh
RUN echo 'CONFIG_FILE=/home/${JUPYTER_USERNAME}/jupyterhub_config.py' >> /opt/entrypoint.sh
RUN echo 'if [ -f "$CONFIG_FILE" ]; then' >> /opt/entrypoint.sh
RUN echo '    echo "conf file exists."'  >> /opt/entrypoint.sh
RUN echo 'else' >> /opt/entrypoint.sh
RUN echo '    mv /root/jupyterhub_config.py /home/${JUPYTER_USERNAME}' >> /opt/entrypoint.sh
RUN echo 'fi' >> /opt/entrypoint.sh
RUN echo '/usr/local/bin/jupyterhub --config=/home/${JUPYTER_USERNAME}/jupyterhub_config.py' >> /opt/entrypoint.sh
RUN chmod +x /opt/entrypoint.sh
RUN chmod 755 /opt/entrypoint.sh

# ENV
ENV SPARK_HOME=/opt/spark-3.0.1-bin-hadoop3.2
RUN ln -s /usr/bin/python3 /usr/bin/python
#scikit-learn and pyarrow do not work
COPY jupyterhub-requirements.txt /root/
RUN pip3 install -r ~/jupyterhub-requirements.txt

RUN touch /etc/profile.d/spark-env.sh
RUN echo '#################################################################' >> /etc/profile.d/spark-env.sh
RUN echo '# SPARK' >> /etc/profile.d/spark-env.sh
RUN echo '#################################################################' >> /etc/profile.d/spark-env.sh
RUN echo 'export SPARK_HOME=/opt/spark-3.0.1-bin-hadoop3.2' >> /etc/profile.d/spark-env.sh
RUN echo 'alias spark-shell="$SPARK_HOME/bin/spark-shell"' >> /etc/profile.d/spark-env.sh
RUN echo 'alias spark-submit="$SPARK_HOME/bin/spark-submit"' >> /etc/profile.d/spark-env.sh
RUN chmod +x /etc/profile.d/spark-env.sh
RUN chmod 755 /etc/profile.d/spark-env.sh


# CLEANUP
RUN apt clean
RUN apt autoremove --purge
RUN rm -r ~/.cache/pip


# EXPOSE PORTS
EXPOSE 8000/tcp
EXPOSE 8001/tcp
EXPOSE 8081/tcp

#scikit-learn
#pyarrow

# DEFINE ENTRY POINT
ENTRYPOINT [ "/opt/entrypoint.sh" ]