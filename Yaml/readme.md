## scp yaml to master node
```bash
ssh node1 sudo mkdir ~/yaml
ssh node1 sudo chown ubuntu:ubuntu ~/yaml
scp D:\Code\RaspberryK8s\_clean\Yaml\* node1:~/yaml
```

## jupyter-hub.yaml
The following section describes the deployment of *ms-sql-edge.yaml* with all it's components

### kubectl commands for ms-sql-edge
```bash
kubectl delete -f ~/yaml/jupyter-hub.yaml
kubectl apply -f ~/yaml/jupyter-hub.yaml
kubectl get all -n jupyter-hub
```

To get log information about a pod use the following command:
```bash
kubectl logs jupyter-hub-5f599c4758-b6c4w -n jupyter-hub
```

## ms-sql-edge.yaml
The following section describes the deployment of *ms-sql-edge.yaml* with all it's components

### kubectl commands for ms-sql-edge
```bash
kubectl delete -f ~/yaml/ms-sql-edge.yaml
kubectl apply -f ~/yaml/ms-sql-edge.yaml
kubectl get all -n ms-sql-edge
```

### login ms-sql-edge
The default password set is *test123456!*. To change this simply replace the value in *ms-sql-edge.yaml* for the *Secret* with the name *ms-sql-edge* in the *data* section the key *SA_PASSWORD*. The value is base64 encoded. to get a base 64 value use the following command:
```bash
echo -n 'test123456!' | base64
```


### ms-sql-edge data
The deployment uses a persistant volume that resides on the nas under the path *\\192.168.1.111\Cluster\ms-sql-edge*. To create a new database do the following steps:
1. Find a .bak file of a sample DB (for example: https://github.com/Microsoft/sql-server-samples/releases)
2. Download the file and put it on the NAS in a sub-folder of *\\192.168.1.111\Cluster\ms-sql-edge* - for example *_backups*
3. Open SSMS and connect to the DB instance with *node1,31443* with user *SA* and the pwd set in the deployment
4. Execute the following script (this works for the file *AdventureWorks2019.bak*) - note the NAS volume is mounted under */var/opt/mssql/*
```sql
USE [master];
GO

RESTORE DATABASE [AdventureWorks]
    FROM  DISK = N'/var/opt/mssql/_backups/AdventureWorks2019.bak'
    WITH  FILE = 1,
    MOVE N'AdventureWorks2017' TO N'/var/opt/mssql/data/AdventureWorks2019.mdf',
    MOVE N'AdventureWorks2017_log' TO N'/var/opt/mssql/data/AdventureWorks2019_log.ldf',
    NOUNLOAD,  STATS = 5
GO
```